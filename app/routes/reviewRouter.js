
const express =require("express");

// Khai báo Middleware

const { getAllReviewMiddleware,
    getReviewsMiddleware,
    postReviewsMiddleware,
    putReviewsMiddleware,
    deleteReviewsMiddleware
} = require("../middlewares/reviewMiddleware.js");

// Tạo ra Router

const reviewRouter = express.Router();

reviewRouter.get("/reviews", getAllReviewMiddleware, (req,res) => {
    res.status(200).json({
        message: "Get All Reviews"
    })
});
reviewRouter.get("/reviews/:reviewId", getReviewsMiddleware,(req,res) => {
    let reviewId = req.params.reviewId;
    res.status(200).json({
        message: `Get a review with reviewId = ${reviewId}`
    })
});
reviewRouter.post("/reviews", postReviewsMiddleware, (req,res) => {
    res.status(201).json({
        message: "Create a review"
    })
});
reviewRouter.put("/reviews/:reviewId", putReviewsMiddleware, (req,res) => {
    let reviewId = req.params.reviewId;
    res.status(200).json({
        message:`Update a review with reviewId = ${reviewId}`
    })
});

reviewRouter.delete("/reviews/:reviewId", deleteReviewsMiddleware, (req,res) => {
    let reviewId = req.params.reviewId;
    res.status(204).json({
        message:`Delete a review with reviewId = ${reviewId}`
    })
});

module.exports = reviewRouter;