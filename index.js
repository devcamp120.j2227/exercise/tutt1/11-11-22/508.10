const express  = require("express");

const mongoose = require("mongoose");
const courseRouter = require("./app/routes/courseRouter");
const reviewRouter = require("./app/routes/reviewRouter");

const app = express();

const port = 8000;

app.use((req,res, next) => {
    console.log(new Date());

    next();
})

mongoose.connect("mongodb://127.0.0.1:27017/CRUD_Course",(error) => {
    if(error) throw error;

    console.log("Connect successfully!");
})
app.get("/", (req,res) => {
    let today = new Date();

    res.status(200).json({
        mesage: `Hôm nay là ngày ${today.getDate()} tháng ${today.getMonth() + 1} năm ${today.getFullYear()}`
    })
})

app.post("/", (req,res,next) => {
    console.log((req.method));

    next();
})

// Khai báo model 
const reviewSchema = require("./app/models/reviewModel.js");
const coursesSchema = require("./app/models/courseModel.js");


app.use("/", courseRouter)
app.use("/" , reviewRouter)
app.listen(port, () => {
    console.log("App listing on port: ", port);
})

