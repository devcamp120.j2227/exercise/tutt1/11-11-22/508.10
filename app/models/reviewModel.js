const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const reviewSchema = new Schema({
   _id: {
        type: mongoose.Types.ObjectId,
        unique: true
   },
   stars: {
        type: Number,
        default: 0 
   },
   note: {
        type: String,
        required: false
   }
}, {
    timestamps: true
})

module.export = mongoose.model("reviews", reviewSchema);

