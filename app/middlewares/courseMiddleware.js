const  getAllCoursesMiddleware = (req,res, next) => {
    console.log("Get all courses");

    next();
}

const  getCoursesMiddleware = (req,res, next) => {
    console.log("Get a courses");

    next();
}

const  postCoursesMiddleware = (req,res, next) => {
    console.log("Create a courses");

    next();
}

const  putCoursesMiddleware = (req,res, next) => {
    console.log("Update a courses");

    next();
}
const  deleteCoursesMiddleware = (req,res, next) => {
    console.log("Delete a courses");

    next();
}

module.exports = {
    getAllCoursesMiddleware,
    getCoursesMiddleware,
    postCoursesMiddleware,
    putCoursesMiddleware,
    deleteCoursesMiddleware
}