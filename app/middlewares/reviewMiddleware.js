const  getAllReviewMiddleware = (req,res, next) => {
    console.log("Get all reviews");

    next();
}

const  getReviewsMiddleware = (req,res, next) => {
    console.log("Get a reviews");

    next();
}

const  postReviewsMiddleware = (req,res, next) => {
    console.log("Create a reviews");

    next();
}

const  putReviewsMiddleware = (req,res, next) => {
    console.log("Update a reviews");

    next();
}
const  deleteReviewsMiddleware = (req,res, next) => {
    console.log("Delete a reviews");

    next();
}

module.exports = {
    getAllReviewMiddleware,
    getReviewsMiddleware,
    postReviewsMiddleware,
    putReviewsMiddleware,
    deleteReviewsMiddleware
}