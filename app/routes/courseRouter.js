
const express =require("express");

// Khai báo Middleware

const {
    getAllCoursesMiddleware,
    getCoursesMiddleware,
    postCoursesMiddleware,
    putCoursesMiddleware,
    deleteCoursesMiddleware
} = require("../middlewares/courseMiddleware.js");

// Tạo ra Router

const courseRouter = express.Router();

courseRouter.get("/courses", getAllCoursesMiddleware, (req,res) => {
    res.status(200).json({
        message: "Get All Courses"
    })
});
courseRouter.get("/courses/:courseId", getCoursesMiddleware,(req,res) => {
    let courseId = req.params.courseId;
    res.status(200).json({
        message: `Get a Course with courseId = ${courseId}`
    })
});
courseRouter.post("/courses", postCoursesMiddleware, (req,res) => {
    res.status(201).json({
        message: "Create a Courses"
    })
});
courseRouter.put("/courses/:courseId", putCoursesMiddleware, (req,res) => {
    let courseId = req.params.courseId;
    res.status(200).json({
        message:`Update a course with courseId = ${courseId}`
    })
});

courseRouter.delete("/courses/:courseId", deleteCoursesMiddleware, (req,res) => {
    let courseId = req.params.courseId;
    res.status(204).json({
        message:`Delete a course with courseId = ${courseId}`
    })
});

module.exports = courseRouter;