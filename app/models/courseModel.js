const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const coursesSchema = new Schema ({
    title : {
        type: String,
        required: false,
        unique: true
    },
    description: {
        type: String,
        required: false
    },
    noStudent: {
        type: Number,
        default: false
    },
    reviews: [
        {
            type: mongoose.Types.ObjectId,
            ref: "reviews"
        }
    ]
}, {
    timestamps: true
})

module.exports = mongoose.model("Course",coursesSchema );